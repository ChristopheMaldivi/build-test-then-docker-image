FROM openjdk:alpine

MAINTAINER Denis Boisset "denis.boisset@orange.com" & Christophe Maldivi "christophe.maldivi@orange.com"

ENV LC_ALL C.UTF-8

# Install our application
COPY *.jar .

# Expose server port
EXPOSE 8080

# Start app
CMD java -jar *.jar
